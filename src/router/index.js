import Vue from 'vue';
import Router from 'vue-router';
import Landing from '@/components/Landing';
import Comics from '@/components/ComicsSection';
import Series from '@/components/SeriesSection';
import Comic from '@/components/Comic';
import Configurar from '@/components/Configurar';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
    },
    {
      path: '/comics',
      name: 'Comics',
      component: Comics,
    },
    /*{
      path: '/comics/:page',
      name: 'Comics',
      component: Comics,
    },*/
    {
      path: '/comics/details/:id',
      name: 'Comics',
      component: Comics,
    },
    {
      path: '/series',
      name: 'Series',
      component: Series,
    },
    /*{
      path: '/comic',
      name: 'comic',
      component: Comic,
    },*/
  ],
});
